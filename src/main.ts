const googlehome = require("google-home-notifier");
const language: string = "ja";
const message: string = "絶対、大丈夫！"

googlehome.device("Google Home", language);

googlehome.notify(message, res => {
  console.log(res);
})
